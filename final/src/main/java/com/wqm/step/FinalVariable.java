package com.wqm.step;

import org.junit.Test;

public class FinalVariable {

	// final 修饰的变量,为常量； 用大写标识；
	@Test
	public void test() {
		int a = 9;
		final int B = 5; // 大写标识为常量，java规范。例如 ：IP_ADDRESS
		System.out.println(a);
		System.out.println(B);

		a = 10;
		B = 6; // b被final 修饰不能被赋值(只能被赋值一次)

	}

	// final 修饰的变量，一定要被初始化,一般被final修饰的成员变量需要用static修饰。因为已经不能被修改了。
	@Test
	public void test1() {
		int a = 9;
		final int B; // 大写标识为常量，java规范。
		final Double NAME = 3.14d;
		final int C;
		System.out.println(a);

		a = 10;
		B = 6; // 初始化常量,一定要初始化。才能调用

		System.out.println(B);
		System.out.println(NAME);// final 修饰的变量为常量，一定要初始化。才能调用
		// System.out.println(C);

		System.out.println(Demo.NUM);
		System.out.println(Demo.COUNT);

	}
}

class Demo {
	// final 修饰成员变量的时候 前面一般加上static，可以直接调用
	static final Double NUM = 19.0000000000000055d; // 双精度  (精确到小数点后15位) 19.000 000 000 000 007
	static final Float COUNT = 19.3125435345F;// 单精度  (精确到小数点后6位)  19.312544
}
