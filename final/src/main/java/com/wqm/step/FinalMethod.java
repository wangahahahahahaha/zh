package com.wqm.step;

import org.junit.Test;

public class FinalMethod {

	@Test
	public void test() {
		Zi zi = new Zi();
		zi.method();

		ZiZi zi2 = new ZiZi();
		zi2.method();
	}
}

class Fu {
	// 加上final,final修饰的方法不能被重写。
	final void method() {
		System.out.println("不能被修改");
	}
}

// 继承有一个弊端：
// 比如我封装好了一个工具类,本来可以直接用嘛。
// 但是如果我继承你的工具类，就可以重写工具类的方法。
// 但是我的本意是不要你修改，你直接用就可以了。
// 这时候就可以用final
class Zi extends Fu {
	@Override
	void method() {
		// super.method();
		System.out.println("修改了");
	}
}

class ZiZi extends Fu {

}