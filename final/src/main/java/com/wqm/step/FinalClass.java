package com.wqm.step;

public class FinalClass {

}

// final修饰的类，不能被继承
final class Fu1 {
	void method () {
		System.out.println("不能被修改");
	}
	
	void method1 () {
		System.out.println("不能被修改");
	}
}

// 不能继承
class Zi1 extends Fu1{
	
}