package com.wqm.step;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.Inet4Address;
import java.net.UnknownHostException;

import org.junit.Test;

// 网络编程也称为Socket编程。 UDP传输用 DatagramSocket,此类用来表示发送和接收数据的套接字。
// socket就是为网络服务提供的一种机制。
// 通信两端都有socket。
// 通信其实就是socket之间的通信。
// 数据在两个Socket之间通过IO传输
public class MySocketUDP {

	// UDP编程。
	// 创建UDP传输的发送端。
	// 思路：
	// 1. 建立UDP的socket服务。
	// 2. 将要发送的数据封装到数据包中。
	// 3. 通过UDP的socket服务，将数据包发送出去。
	// 4. 关闭socket服务。
	@Test
	public void sendUDP() throws UnknownHostException, IOException {
		System.out.println("发送端。。。。。。");
		while (true) {
			// 建立UDP的socket服务。
			DatagramSocket socket = new DatagramSocket();
			// 将要发送的数据封装到数据包中，并指定接收端 具体ip和端口接收数据
			String str = "你好！！！，陈顺华。My name is WangQinMin";
			/* 发送端：指定ip地址则表示发送到具体ip地址，如果ip最后网段为255，那么为广播该网段所有地址 */
			DatagramPacket packet = new DatagramPacket(str.getBytes(), str.getBytes().length,
					Inet4Address.getByName("192.168.2.255"), 9999);
			// 通过UDP的socket服务，将数据包发送出去。
			socket.send(packet);
			// 关闭socket服务。
			socket.close();
		}
	}

	// UDP编程。
	// 创建UDP传输的接收端。
	// 思路：
	// 1. 建立UDP的socket服务,因为要接收别人发送指定ip地址的数据所以必须明确端口号
	// 2. 创建数据包，用户存储数据。方便用数据包对象，解析数据内容。
	// 3. 使用socket服务的receive方法将接收的数据存储到数据包中。
	// 4. 通过数据包对象的方法，解析其中的数据，比如： 地址，端口，数据内容
	// 5. 关闭socket服务。
	@Test
	public void receiveUDP() throws Exception {
		System.out.println("接收端。。。。。。");
		while (true) {
			// 建立UDP的socket服务,指定要接收数据的ip 和 端口号
			DatagramSocket socket = new DatagramSocket(9999);
			// 创建数据包，用户存储数据。方便用数据包对象，解析数据内容。
			byte[] buf = new byte[1024];
			DatagramPacket packet = new DatagramPacket(buf, buf.length);
			// 使用接收方法将数据存储到数据包中
			socket.receive(packet);
			// 使用数据包对象，将数据解析出来 地址，端口，数据内容
			String ip = packet.getAddress().getHostAddress();
			int port = packet.getPort();
			byte[] data = packet.getData();
			System.out.println(ip + ":" + port);
			System.out.println(new String(data, 0, data.length));
			// 关闭socket服务。
			socket.close();
		}
	}

}
