package com.wqm.step;

import java.io.UnsupportedEncodingException;
import java.net.Inet4Address;
import java.net.Inet6Address;
import java.net.InetAddress;
import java.net.UnknownHostException;

import org.junit.Test;

/**
 * 
 * @author Administrator
 *
 */
// java.net.InetAddress 此类表示IP地址的操作类。
// 属于网络模型中的网际层。对ip地址的封装与解封装。
public class MyInetAddress {

	/**
	 * 获取本地主机名及ip地址
	 * 
	 * @throws UnknownHostException
	 */
	@Test
	public void test1() throws UnknownHostException {
		InetAddress localHost = InetAddress.getLocalHost();
		System.out.println(localHost);// PC-20180416FMFZ/192.168.2.168
		InetAddress localHost2 = Inet4Address.getLocalHost();
		System.out.println(localHost2);// PC-20180416FMFZ/192.168.2.168
		InetAddress localHost3 = Inet6Address.getLocalHost();
		System.out.println(localHost3);// PC-20180416FMFZ/192.168.2.168
		System.out.println("-----------------------------");
		System.out.println(localHost.getHostAddress());// 192.168.2.168
		System.out.println(localHost.getHostName());// PC-20180416FMFZ
		System.out.println(localHost.getCanonicalHostName());// PC-20180416FMFZ
		System.out.println(localHost.getAddress());// 返回字节数组
	}

	/**
	 * 域名解析，先在本地的host文件中查询，如果没有的话就会去互联网上查找。
	 * 
	 * @throws UnknownHostException
	 */
	@Test
	public void test2() throws UnknownHostException {
		// InetAddress allByName = InetAddress.getByName("180.97.33.108");
		// InetAddress allByName = InetAddress.getByName("PC-20180416FMFZ");
		InetAddress allByName = InetAddress.getByName("tieba.baidu.com");// tieba.baidu.com/14.215.177.221
		System.out.println(allByName);// www.baidu.com/180.97.33.107

		// InetAddress loopbackAddress = Inet4Address.getLoopbackAddress();
		// System.out.println(loopbackAddress.getHostAddress());// 127.0.0.1

	}
}
