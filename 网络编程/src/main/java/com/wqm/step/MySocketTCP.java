package com.wqm.step;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;

import org.junit.Test;

// 网络编程也称为Socket编程。
// socket就是为网络服务提供的一种机制。
// 通信两端都有socket。
// 通信其实就是socket之间的通信。
// 数据在两个Socket之间通过IO传输
public class MySocketTCP {

	// TCP传输，客户端建立的过程解析：
	// 1.创建TCP客户端socket服务，使用的是socket对象。(建议该对象一创建就明确目的地址,要连接的ip和端口)
	// 2.如果连接建立成功，说明数据传输通道以建立。 (该通道就是socket流，底层建立好的。)
	// 该通道既可以输入也可以输出，想要输入输出流对象，可以用socket来操作。
	// 3. 获取通道的输入流 或 输出流
	// 4. 关闭资源
	@Test
	public void TCPClient() throws UnknownHostException, IOException {
		System.out.println("客户端。。。。。。");
		while (true) {
			// 创建TCP客户端socket服务,指定要连接的ip和端口
			Socket socket = new Socket("127.0.0.1", 8888);
			// 获取该通道的输入流
			// InputStream inputStream = socket.getInputStream();
			// 获取该通道的输出流
			OutputStream outputStream = socket.getOutputStream();
			// 将数据写出到网络
			outputStream.write("你好，我又来了额。。".getBytes());
			// 传输完成后，关闭资源
			socket.close();
		}
	}

	// 建立TCP服务端的思路：
	// 1.创建服务端socket服务，通过ServerSocket对象。
	// 2.服务端必须对外提供一个端口，否则客户端无法连接。 例如： 连接数据库 ip、port、username、password
	// 3.获取连接过来的客户端对象。
	// 4.通过客户端对象获取socket流读取客户端发来的数据。并打印在控制台。
	// 5. 关闭资源 (关闭客户端，关闭服务端)
	@Test
	public void TCPServer() throws UnknownHostException, IOException {
		System.out.println("服务端。。。。。。");
		while (true) {
			ServerSocket serverSocket = new ServerSocket(8888);
			// 获取连接过来的客户端对象。
			Socket s = serverSocket.accept();
			String ip = s.getInetAddress().getHostAddress();
			// 通过客户端对象获取socket流读取客户端发来的数据
			InputStream inputStream = s.getInputStream();
			byte[] buf = new byte[1024];
			int len = inputStream.read(buf);
			String text = new String(buf, 0, len);
			System.out.println(ip + ":" + text);

			s.close();
			serverSocket.close();
		}
	}

}
