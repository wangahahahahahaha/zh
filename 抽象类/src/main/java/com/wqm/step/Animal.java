package com.wqm.step;

/**
 * 
 * @author Administrator
 * 
 */
/* 有抽象方法，该类一定是抽象类 */
/* 抽象类中不定有抽象方法 */
/* 抽象类不能被实例化*/
public abstract class Animal {

	abstract void show(); // 抽象方法

	void method() {
		System.out.println("公共的具体实现的方法");
	}
}
