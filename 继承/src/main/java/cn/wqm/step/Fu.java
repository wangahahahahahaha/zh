package cn.wqm.step;

public class Fu {

	private Integer age;
	// 继承则可以访问的权限
	protected String name;

	// 局部代码块，
	{
		System.out.println("局部代码块赋值");
		name = "儿子";
	}

	protected void eat() {
		System.out.println("吃西瓜");
	}

	public Integer getAge() {
		return age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}

	public String getName() {
		System.out.println("获取名字");
		return name;
	}

	public void setName(String name) {
		System.out.println("设置名字");
		this.name = name;
	}

}
