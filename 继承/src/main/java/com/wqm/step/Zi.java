package com.wqm.step;

import org.junit.Test;

import cn.wqm.step.Fu;

public class Zi extends Fu {

	@Test
	public void test() {
		eat();
		// 继承之后直接输出 ： null
		System.out.println(name);
	}

	// 子类可以重写父类的方法
	// 重写方法的修饰符权限不能比父类小
	@Override
	protected void eat() {
		// super.eat();
		System.out.println("吃哈密瓜");
	}
}
