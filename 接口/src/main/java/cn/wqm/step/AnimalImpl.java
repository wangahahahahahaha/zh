package cn.wqm.step;

import com.wqm.step.Animal;
import com.wqm.step.Dog;

public class AnimalImpl implements Dog, Animal {

	// 多实现中的两个show1()方法，这里表现只实现一个。
	public void show1() {
		System.out.println(Animal.COUNT);
		System.out.println(Animal.NUM);
	}

	public void show2() {

	}

	public void method1() {

	}

	public void method2() {

	}

	@Override
	public int method() {  // 多实现中，不能有两个  方法相同，返回值类型不同的方法。
		return 0;
	}

}
