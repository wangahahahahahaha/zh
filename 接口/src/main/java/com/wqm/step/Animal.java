package com.wqm.step;

// 接口中常见的成员：
// 		全局常量： public static final 
//		抽象方法： public abstract
public interface Animal {

	public static final int NUM = 4; // int 前面也可以省略，默认public static final

	public abstract void show1();// void 前面可以省略，默认public abstract

	public abstract void show2();

	public abstract int method();

	// 例如： 虽然简化了但是阅读性极差
	int COUNT = 8;

	void method1(); // 从不同包下调用可以证明了这一点。 (权限修饰符作用范围)

	void method2();

}
