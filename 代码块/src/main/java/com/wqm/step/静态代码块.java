package com.wqm.step;

public class 静态代码块 {

	static int a = 5;

	public static int getA() {
		return a;
	}

	public static void setA(int a) {
		静态代码块.a = a;
	}

	// 静态代码块赋值 ： jdbc连接驱动用到过
	static {
		a = 10;
		System.out.println(a); // 10
	}

	public static void main(String[] args) {
		// 静态方法调用
		静态代码块.setA(40);
		System.out.println(a);// 40
	}
}
