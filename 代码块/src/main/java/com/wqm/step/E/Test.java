package com.wqm.step.E;

import com.wqm.step.Animal;
import com.wqm.step.Person;

public class Test {

	// 测试默认修饰符，修饰构造函数 (默认修饰符，同包下可以访问)
	@org.junit.Test
	public void test() {
		Person person = new Person();
	}

	// 构造代码块,其实就是构造函数
	// 与静态代码块的区别就是： 静态代码块随类的加载而加载，构造代码块随类的创建而初始化
	@org.junit.Test
	public void test1() {
		Animal a = new Animal();
		System.out.println(a.getName());
		
	}
}
