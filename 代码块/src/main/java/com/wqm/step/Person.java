package com.wqm.step;
/**
 * 构造函数：就是初始化对象，给对象赋值。不要想多了。
 * @author Administrator
 *
 */
public class Person {

	private String name;
	
	// 无参构造函数
//	public Person() {
//		super();
//	}
	
	Person() {
		super();
	}

	// 带参的构造函数
	public Person(String name) {
		super();
		this.name = name;
	}

	
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
}
