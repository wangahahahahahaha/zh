package com.wqm.step;

import org.junit.Test;

public class 局部代码块 {

	@Test
	public void test() {
		System.out.println(func(4));
	}

	public Integer func(Integer a) {
		// 释放定义变量的内存空间
		{
			int q = 10;
			int w = 20;
			int e = 30;
			int r = 40;
			return a + q + w + e + r;
		}
	}
}
