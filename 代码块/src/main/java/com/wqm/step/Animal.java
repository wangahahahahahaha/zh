package com.wqm.step;

// 演示构造代码块，解析其作用
public class Animal {

	private String name;

	// 构造代码块 (构造函数)
	public Animal() {
		name = "baby";
	}

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
}
